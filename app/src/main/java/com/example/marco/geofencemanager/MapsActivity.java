package com.example.marco.geofencemanager;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;



public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        LocationListener, GoogleApiClient.OnConnectionFailedListener, OnStreetViewPanoramaReadyCallback {

    private LinearLayout linearLayout;
    private GoogleMap mMap;
    private boolean first=true;
    private Menu menu;
    private boolean check=false;
    private GoogleApiClient mGoogleApiClient;
    private GeofencingClient mGeofencingClient;
    private ArrayList<Geofence> geofences = new ArrayList<>();
    private StreetViewPanorama svp;
    private float bear;
    private ArrayList<GeofenceModel> geofencesList = new ArrayList<>();
    private HashMap<String, Circle> allCircle = new HashMap<>();
    private HashMap<String, Marker> allMarker = new HashMap<>();
    private ArrayList<String> elementi=new ArrayList<>();
    private int index = 0;//contatore Geofences per navigazione

    DBhelper dBhelper;
    private DatabaseReference ref;//ref to firebase

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Singleton.setContext(this);
        dBhelper = new DBhelper(Singleton.getContext());
        dBhelper.retrivingTransition();
        //continua se il gps è ON e se c'è una connessione internet
        if (turnGPSOn() && InternetConnection.haveInternetConnection(MapsActivity.this)) {
            Log.e("CONNESSIONE INT/GPS", "Presenti");
            if (!checkLocationPermission()) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else continua();


        } else {//altrimenti segnalo assenza gps o internet
            popupAttention();
        }

    }

    //popup se internet e gps non attivi
    private void popupAttention() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.attention);
        builder.setMessage(R.string.gpsOff);
        builder.setPositiveButton(getString(R.string.provaDinuovo),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        first=true;
                        MapsActivity.this.recreate();
                    }
                });
        builder.setNegativeButton(R.string.exitApp, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MapsActivity.this.finish();
            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //qui se gps e internet attivi
    private void continua() {

        Singleton.setElimina(false);//reset variabile elimina
        Singleton.setModifica(false);//reset variabile modifica
        Singleton.setAttivazioneNotifiche(false);

        //creo fragment mappa
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //collegamento a database firebase e scaricamento geofences
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        ref = database.getReference();
        ref.addChildEventListener(new ChildEventListener() {

            //  appena in firebase viene aggiunta geofence...
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                //creo geofenceModel (vedi classe)
                GeofenceModel newGeofence = dataSnapshot.getValue(GeofenceModel.class);
                //aggiungo alla lista delle geofence
                geofencesList.add(newGeofence);
                //creo geofence con API,cerchio e marker
                creaGeofence(newGeofence);
                //setto posizione corrente
                Singleton.setPosition(newGeofence.getPos());
                //notifico la creazione
                notifyChangement(newGeofence.getId());

            }

            //se viene modificato il database...
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                GeofenceModel g = dataSnapshot.getValue(GeofenceModel.class);
                //notifico modifica
                notifyChangement(g.getId());
                Singleton.setPosition(g.getPos());
                Singleton.setModifica(true);//è una modifica
                Singleton.setGeofDaModificare(g.getId());
                //modifico cerchio e transizione su mappa
                modificaGeofence(g);
            }

            //se viene eliminata una geofence dal database...
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.e("Eliminazione", "DATABASE");
                GeofenceModel gDaEliminare = null;
                Singleton.setElimina(true);//è un eliminazione
                GeofenceModel geofence = dataSnapshot.getValue(GeofenceModel.class);
                //notifico eliminazione
                notifyChangement(geofence.getId());
                //elimino tale geofence da mappa e da API
                eliminaGeofence(geofence.getId(),true);
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.e("moved", dataSnapshot.getValue(GeofenceModel.class).getId());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("onCancelled", String.valueOf(databaseError));
            }


        });
        //setto bottone navigazione
        Button nav = (Button) findViewById(R.id.nav);
        nav.setVisibility(View.VISIBLE);
        //Setto click navigatore Geofences
        nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!geofencesList.isEmpty()) {
                    //se ho geofence caricate muovi camera e svp verso prossima geofence
                    Log.e("geofencesize", String.valueOf(geofencesList.size()));
                    if (index >= geofencesList.size())
                        index = 0;
                    allMarker.get(geofencesList.get(index).getId()).showInfoWindow();
                    CameraPosition cm = new CameraPosition.Builder()
                            .target(geofencesList.get(index).getPos()).tilt(45).zoom(16).build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cm), 1000, null);
                    svp.setPosition(geofencesList.get(index).getPos());
                    index++;
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu, this.menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            //case cambiamento impostazioni
            case R.id.start://attivazioni notifiche geofence
                for(GeofenceModel g:geofencesList){
                    MenuItem register = menu.findItem(R.id.start);
                    MenuItem register2 = menu.findItem(R.id.stop);
                    if (g.getAttivata()){
                        register.setVisible(false);
                        register2.setVisible(true);
                        CostruisciGeofence(g);
                        geofencesRequest();
                    }
                }
                Singleton.setAttivazioneNotifiche(true);
                Toast.makeText(getApplicationContext(), getString(R.string.activate), Toast.LENGTH_SHORT).show();
                break;
            case R.id.stop://disattivazione notifiche

                ArrayList<String> ids = new ArrayList<>();
                for(GeofenceModel g:geofencesList){
                    MenuItem register = menu.findItem(R.id.start);
                    MenuItem register2 = menu.findItem(R.id.stop);
                    if (g.getAttivata()){
                        register.setVisible(true);
                        register2.setVisible(false);
                        ids.add(g.getId());
                    }
                }
                mGeofencingClient.removeGeofences(ids).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.i("eliminato", "eliminato");
                        Toast.makeText(getApplicationContext(), getString(R.string.disactivate), Toast.LENGTH_SHORT).show();
                        Singleton.setAttivazioneNotifiche(false);
                    }
                });
                break;
            case R.id.report:
                //dBhelper.retrivingTransition();
                if(Singleton.getReport().contains("") || Singleton.getReport().isEmpty()){
                    Toast.makeText(getApplicationContext(), getString(R.string.noReport), Toast.LENGTH_SHORT).show();
                    Singleton.getReport().remove("");
                }
                else
                    creaReport();
                break;
            case R.id.delete://rimunove tutte le geofences
                for(GeofenceModel g : geofencesList)
                    ref.child(g.getId()).removeValue();
                break;
        } return super.onOptionsItemSelected(item);
    }

    private void creaReport() {

        ArrayAdapter<String> adapter= new ArrayAdapter<>(MapsActivity.this,android.R.layout.simple_list_item_1,Singleton.getReport());
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.elenco));
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ChangeView(Singleton.getReport().get(which));
                Log.e("which", String.valueOf(which));
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void ChangeView(String report) {
        Log.e("report",report.split("     -     ")[1]);

        String id=report.split("     -     ")[1].split("->")[0];
        Log.e("id",report.split("     -     ")[1].split("->")[1]);
        for (GeofenceModel g : geofencesList)
            if(id.equals(g.getId())) {
                allMarker.get(id).showInfoWindow();
                CameraPosition cm = new CameraPosition.Builder()
                        .target(g.getPos()).tilt(45).zoom(16).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cm), 1000, null);
                svp.setPosition(g.getPos());
            }
    }

    //creazione notifica cambiamento stato geofence
    private void notifyChangement(String id) {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MapsActivity.this)
                .setSmallIcon(android.R.drawable.stat_notify_error)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE)
                .setContentTitle(getString(R.string.notifyChange));
        if (Singleton.getModifica())
            notificationBuilder.setContentText(getString(R.string.notifyChanged) + ":" + id);
        else if (Singleton.getElimina())
            notificationBuilder.setContentText(getString(R.string.notifyDelete) + ":" + id);
        else
            notificationBuilder.setContentText(getString(R.string.notifyCreate) + ":" + id);
        Notification mNot = notificationBuilder.build();
        NotificationManager mNotMan = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotMan.notify(1, mNot);
    }

    //controllo se gps attivo
    private Boolean turnGPSOn() {
        int locationMode = 0;
        try {
            locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }

    //Cerco permessi per accedere alla posizione
    public boolean checkLocationPermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

    }

    //se viene fatta una richiesta di permessi...
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    check= true;

                    Log.e("Creazione", "street");

                } else {
                    check=false;
                    Log.e("no permission", "no party");
                    MapsActivity.this.finish();
                }
            }
        }
        if (check) continua();
        //MapsActivity.this.recreate();
    }

    //inizializzo servizi location API
    private void googleApiInit() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleApiInit();
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                //setto latlng premuta in singleton
                Singleton.setPosition(latLng);
                //zoom sul punto del tocco
                CameraPosition cm = new CameraPosition.Builder()
                        .target(latLng).tilt(45).zoom(16).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cm), 1000, null);

                svp.setPosition(latLng);
                creaDialog();
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //se clicco marker dialog al click e preparo marker e geofence da modificare in base
                //a posizione uguale tra lista marker e geofence
                Singleton.setMarkerDaModificare(marker);

                for (GeofenceModel g : geofencesList) {
                    if (g.getPos().equals(marker.getPosition())) {
                        Singleton.setGeofDaModificare(g.getId());
                        Singleton.setAttivata(g.getAttivata());
                        Singleton.setPosition(g.getPos());
                        svp.setPosition(g.getPos());
                        Log.e("pos", g.getId());
                    }
                }
                dialogModifica();
                return false;
            }
        });

        mMap.setMyLocationEnabled(true);
        //streetView creation
        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama streetViewPanorama) {
        // se muovo svp allora muovo in modo sincronizzato anche mappa
        svp = streetViewPanorama;
        if (Singleton.getPosition() != null)
            svp.setPosition(Singleton.getPosition());
        svp.setOnStreetViewPanoramaCameraChangeListener(new StreetViewPanorama.OnStreetViewPanoramaCameraChangeListener() {
            @Override
            public void onStreetViewPanoramaCameraChange(StreetViewPanoramaCamera svpCamera) {
                Log.e("curLocSVP", String.valueOf(Singleton.getPosition()));
                bear = svpCamera.bearing;
                CameraPosition cp = new CameraPosition.Builder().bearing(bear)
                        .target(Singleton.getPosition()).zoom(16).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp), 100, null);
            }
        });

    }

    //richiesta nuova geofence API
    private void geofencesRequest() {
        GeofencingRequest gr = new GeofencingRequest.Builder().addGeofences(geofences)

                // .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER|GeofencingRequest.INITIAL_TRIGGER_DWELL)
                .build();

        // creo pending intent per trigger geofence
        Intent intent = new Intent(this, MyGeofenceService.class);
        PendingIntent pIntent = PendingIntent.getService(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Log.e("INTENT", "REQUEST");
        // registro GeofenceRequest

        mGeofencingClient.addGeofences(gr,pIntent);
    }



    private void creaDialog() {
        //dialog al click mappa (no marker)
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.creation, null);
        dialogBuilder.setView(dialogView);
        final EditText radius = dialogView.findViewById(R.id.radius);//Definisco EditText della dialog per il raggio in metri
        final EditText idText = dialogView.findViewById(R.id.id); //Definisco Edit text per l'ID della Geofence
        final EditText durata = dialogView.findViewById(R.id.durata); //Definisco Edit text per la durata della transition

        if (Singleton.getModifica()) {
            idText.setVisibility(View.GONE);//se è modifica non mostro text per ID
        } else idText.setVisibility(View.VISIBLE);

        //Definisco pulsanti check
        final CheckBox entrata = dialogView.findViewById(R.id.entrata);
        final CheckBox uscita =  dialogView.findViewById(R.id.uscita);
        final CheckBox permanenza =  dialogView.findViewById(R.id.persistenza);

        dialogBuilder.setTitle(R.string.insertGeofence) //titolo dialog
                .setCancelable(false);
        //Pulsante affermativo
        dialogBuilder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //controllo tipo di transizione scelta dall'utente
                ArrayList<Integer> transizioni = new ArrayList<>();

                if (uscita.isChecked())
                    transizioni.add(Geofence.GEOFENCE_TRANSITION_EXIT);
                if (entrata.isChecked())
                    transizioni.add(Geofence.GEOFENCE_TRANSITION_ENTER);
                if (permanenza.isChecked())
                    transizioni.add(Geofence.GEOFENCE_TRANSITION_DWELL);

                if (transizioni.isEmpty()) {
                    Toast.makeText(getApplicationContext(), getString(R.string.unCheck), Toast.LENGTH_SHORT).show();

                } else {
                    //se è una creazione...
                    if (!Singleton.getModifica()) {
                        //Se almeno un campo è vuoto
                        if (radius.getText().toString().isEmpty() ||durata.getText().toString().isEmpty()
                                ||idText.getText().toString().isEmpty()) {
                            Toast.makeText(getApplicationContext(), getString(R.string.impossible), Toast.LENGTH_SHORT).show();
                        } else if (controlExistence(idText.getText().toString())) {
                            Toast.makeText(getApplicationContext(), getString(R.string.ErrorExist), Toast.LENGTH_SHORT).show();
                        } else {
                            //se inserimento ok allora aggiungo new geofence in DB
                            addToFirebase(new GeofenceModel(idText.getText().toString(),
                                    Singleton.getPosition().latitude,
                                    Singleton.getPosition().longitude,
                                    radius.getText().toString(), transizioni,Integer.parseInt(durata.getText().toString()),true));
                        }
                    }
                    //altrimenti è una modifica...
                    else if (radius.getText().toString().isEmpty() ||durata.getText().toString().isEmpty()){
                        Toast.makeText(getApplicationContext(), getString(R.string.impossibleMod), Toast.LENGTH_SHORT).show();
                    } else {
                        //se inserimento è ok modifico geofence su DB
                        DatabaseReference change = ref.child(Singleton.getGeofDaModificare());
                        change.child("raggio").setValue(radius.getText().toString());
                        change.child("transizione").setValue(transizioni);
                        change.child("time").setValue(Integer.parseInt(durata.getText().toString()));

                    }
                }
            }
        });

        //Pulsante negativo
        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Singleton.setModifica(false);
            }
        });

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    //controllo se id già presente
    private boolean controlExistence(String s) {
        for (GeofenceModel g : geofencesList) {
            if (g.getId().equals(s)) return true;
        }
        return false;
    }

    //dialog di scelta tra modifica e elimina
    private void dialogModifica() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getString(R.string.attention));
        alert.setMessage(getString(R.string.editOrDelete));
        //Se sceglie di modificare
        alert.setPositiveButton(getString(R.string.edit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Singleton.setModifica(true);
                creaDialog();
            }
        });
        if(Singleton.getAttivata()){ //se geofence attivata possibilita disattivarla
            alert.setNeutralButton(getString(R.string.disattiva),new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Singleton.setModifica(true);
                    disattivaGeofence();
                    Toast.makeText(getApplicationContext(), getString(R.string.disattivata), Toast.LENGTH_SHORT).show();

                }
            });}
        else{//se disattivata possibilita attivarla
            alert.setNeutralButton(getString(R.string.attiva),new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Singleton.setModifica(true);
                    attivaGeofence();
                    Toast.makeText(getApplicationContext(), getString(R.string.attivata), Toast.LENGTH_SHORT).show();

                }
            });}

        //Se sceglie di eliminare
        alert.setNegativeButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ref.child(Singleton.getGeofDaModificare()).removeValue();//rimuovo geofence firebase
            }
        });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    private void disattivaGeofence() {
        Singleton.setAttivata(false);
        DatabaseReference change = ref.child(Singleton.getGeofDaModificare());
        change.child("attivata").setValue(false);
    }

    private void attivaGeofence() {
        Singleton.setAttivata(true);
        DatabaseReference change = ref.child(Singleton.getGeofDaModificare());
        change.child("attivata").setValue(true);
    }

    //modifico geofence con dati immessi in dialog
    private void modificaGeofence(GeofenceModel geofence) {
        LatLng latlng;
        //elimino vecchia geofence API, e cerchio
        eliminaGeofence(geofence.getId(),false);
        geofencesList.add(geofence);
        Log.e("allCircle.size()", String.valueOf(allCircle.size()));
        allCircle.get(geofence.getId()).remove();
        latlng = new LatLng(geofence.getLat(), geofence.getLon());
        //creo e associo nuovo cerchio
        if(geofence.getAttivata()){
            allCircle.put(geofence.getId(), creaCerchio(latlng, Integer.parseInt(geofence.getRaggio()),true));
            if(Singleton.getAttivazioneNotifiche()){
                //costruisco geofence
                CostruisciGeofence(geofence);
                //Richiesta di geofences
                geofencesRequest();}
        }
        else
            allCircle.put(geofence.getId(), creaCerchio(latlng, Integer.parseInt(geofence.getRaggio()), false));

        //modifico lo snippet del marker
        String s = "|";
        for (int i : geofence.getTransizione()) {
            if (i == 1)
                s = s + getString(R.string.enter) + "|";
            if (i == 2)
                s = s + getString(R.string.exit) + "|";
            if (i == 4)
                s = s + getString(R.string.dwell) + "|";
        }

        //al click sul marker ho salvato il marker da modificare
        Singleton.getMarkerDaModificare().setSnippet(s);
        Singleton.getMarkerDaModificare().showInfoWindow();

        //costruisco geofence...
        // CostruisciGeofence(geofence);
        //Richiesta di geofences API
        //  geofencesRequest();
        Singleton.setModifica(false);//reset variabile modifica

    }


    private void creaGeofence(GeofenceModel geofence) {
        //converto input del raggio in intero
        int raggio = Integer.parseInt(geofence.getRaggio());
        Log.e("raggio", String.valueOf(raggio));
        Log.e("lat", String.valueOf(geofence.getLat()));


        //creo marker geofence
        Marker actualMarker = mMap.addMarker(new MarkerOptions()
                .position(geofence.getPos())
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .title(geofence.getId()));
        String s = "|";
        for (int i : geofence.getTransizione()) {
            if (i == 1)
                s = s + getString(R.string.enter) + "|";
            if (i == 2)
                s = s + getString(R.string.exit) + "|";
            if (i == 4)
                s = s + getString(R.string.dwell) + "|";
        }
        actualMarker.setSnippet(s);
        //associo marker a geofence
        allMarker.put(geofence.getId(), actualMarker);
        //creo cerchio per visibilità della geofence e li associo
        if(geofence.getAttivata()) {
            allCircle.put(geofence.getId(), creaCerchio(geofence.getPos(), raggio, true));
            if(Singleton.getAttivazioneNotifiche()){
                //costruisco geofence
                CostruisciGeofence(geofence);
                //Richiesta di geofences
                geofencesRequest();}
        }
        else{
            Singleton.setAttivata(false);
            allCircle.put(geofence.getId(), creaCerchio(geofence.getPos(), raggio, false));
        }
        Log.e("cerchio", allCircle.get(geofence.getId()).toString());
    }

    private void CostruisciGeofence(GeofenceModel geofence) {
        geofences.clear();
        Geofence.Builder builder = new Geofence.Builder()
                .setRequestId(geofence.getId())
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setLoiteringDelay(geofence.getTime()*1000)
                .setCircularRegion(geofence.getLat()
                        , geofence.getLon(), Integer.parseInt(geofence.getRaggio()));

        int ris=geofence.getTransizione().get(0);
        for(int i=1;i<geofence.getTransizione().size();i++){
            ris=ris+geofence.getTransizione().get(i);
        }
        builder.setTransitionTypes(ris);
        geofences.add(builder.build());
    }

    //aggiunge geofence a DB
    private void addToFirebase(GeofenceModel geofence) {
        ref.child(geofence.getId()).setValue(geofence);
    }


    //crea cerchio su mappa
    private Circle creaCerchio(LatLng pos, int raggio,boolean attivata) {
        CircleOptions circleOptions = new CircleOptions()
                .center(pos)    // posizione del cerchio
                .radius(raggio)  // raggio in metri
                ;  // raggio in metri
        if(attivata)
            circleOptions.fillColor(0x40ff0000);
        else
            circleOptions.fillColor(0x40f188f0);

        circleOptions.strokeColor(Color.TRANSPARENT)
                .strokeColor(Color.TRANSPARENT)
                .strokeWidth(2);

        // disegno su mappa
        return mMap.addCircle(circleOptions);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mGeofencingClient=LocationServices.getGeofencingClient(MapsActivity.this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        //Prendo posizione corrente
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        LatLng posizione = new LatLng(lat, lon);
        Log.e("posizione", String.valueOf(posizione));
        CameraPosition cm = new CameraPosition.Builder()
                .target(posizione).tilt(45).zoom(16).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cm), 3000, null);
        svp.setPosition(posizione);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //elimina la geofence (solo API e cerchio se modifica) se true allora è eliminazione else una modifica
    private void eliminaGeofence(String geofence,boolean elimina) {
        Log.e("Eliminazione", String.valueOf(Singleton.getElimina()));
        ArrayList<String> ids = new ArrayList<>();
        ids.add(geofence);

        Geofence el = null;
        GeofenceModel ge = null;
        //se è solo una modifica

        for (GeofenceModel g : geofencesList)
            if (g.getId().equals(geofence))
                ge = g;
        geofencesList.remove(ge);//rimuovo geofence locale
        //se elimina true rimuovo tutto
        if (elimina) {
            Log.e("elimino", "true");
            allCircle.get(geofence).remove();
            allCircle.remove(geofence);//rimuovo cerchio
            allMarker.get(geofence).remove();//rimuovo marker da mappa
            allMarker.remove(geofence);//rimuovo marker
            Singleton.setElimina(false);//reset eliminazione
        }
        //rimuovo servizio geofence
        mGeofencingClient.removeGeofences(ids).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.i("eliminato", "eliminato");
            }
        });


    }

}
