package com.example.marco.geofencemanager;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DBhelper extends SQLiteOpenHelper{

    //Stringa per controllo log
    private static final String LOG = "DBhelper";

    //Versione Db
    private static final int DATABASE_VERSION = 1;

    //Nomi dataB
    private static final String TABLE_NAME = "geofence";
    private static final String COL_ID = "id";
    private static final String COL_TRANSITION = "transition";
    private static final String COL_TIME = "time";

    //String creazione
    private static final String CREATE_TABLE = "CREATE TABLE geofence (_id integer primary key autoincrement,"
            + COL_ID+" string, "+ COL_TRANSITION +" string, "+COL_TIME+" text )";

    private static final String DEL_TABLE = "DROP TABLE geofence";


    DBhelper(Context ctx){
        super(ctx,TABLE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase){
        Log.w(LOG,"onCreate");
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,int i,int i2){
        sqLiteDatabase.execSQL(DEL_TABLE);
        onCreate(sqLiteDatabase);
    }

    //add degli elementi nel database
    void addTransition() {
        ContentValues values = new ContentValues();
        for (String s : Singleton.getReport()) {
            String[] tmp1 = s.split(" {5}-{5}");
            for (String aTmp1 : tmp1) {
                if (aTmp1.contains("In uscita") || aTmp1.contains("In entrata") || aTmp1.contains("Persistenza")) {
                    String[] tmp2 = aTmp1.split("->");
                    values.put(COL_ID, tmp2[0]);
                    values.put(COL_TRANSITION,tmp2[1]);
                } else {
                    values.put(COL_TIME, aTmp1);
                }
            }
            SQLiteDatabase db = getWritableDatabase();
            long ids = db.insert(DBhelper.TABLE_NAME, null, values);
            db.close();
            Log.w(LOG, "Insertion Success of " + ids);
        }
    }




        //Retriving dei report
    void retrivingTransition(){
        String [] colum ={COL_ID,COL_TRANSITION,COL_TIME};

        try {
            SQLiteDatabase db = getReadableDatabase();
            @SuppressLint("Recycle") Cursor c = db.query(TABLE_NAME,colum,null,null,null,null,null);
            String s="";

            while (c.moveToNext()){
                String ID = c.getString(c.getColumnIndexOrThrow(COL_ID));
                String transition = c.getString(c.getColumnIndexOrThrow(COL_TRANSITION));

                String []stt = s.split("/-/");
                for (String s1:stt) {
                    String s2=ID + "->" + transition;
                    if(!s1.equals(s2)) s +=ID + "->" + transition+"/-/";
                }

                Log.w(LOG, "RETRIVING : "+ s);
            }
            String [] split = s.split("/-/");
            for (String st:split) {
                Singleton.getReport().add(st);
            }

        }catch (Throwable t){
            Log.e(LOG, "getRows: "+t.toString(),t);
        }

    }

}
