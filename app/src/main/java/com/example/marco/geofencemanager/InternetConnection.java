package com.example.marco.geofencemanager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetConnection {

    //controllo internet attivato
    public static boolean haveInternetConnection(Context contesto) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) contesto.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo==null) return false;
        if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
            if (netInfo.isConnected())
                haveConnectedWifi = true;//se wifi allora true
        if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
            if (netInfo.isConnected())
                haveConnectedMobile = true;//se mobile allora true

        return haveConnectedWifi || haveConnectedMobile;
    }

}
