package com.example.marco.geofencemanager;

import android.content.Context;

import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

public class Singleton {
    private static Context context; //context attuale
    private static ArrayList<String> report=new ArrayList<>();
    private static LatLng position; //posizione attuale
    private static boolean modifica; // true se si sta modificando geofence
    private static boolean elimina; //true se si sta eliminando geofence
    private static boolean attivata; //true se si sta attivando geofence
    private static boolean attivazioneNotifiche;//true se notifiche attivate
    private static String GeofDaModificare; //geofence da modificare
    private static Marker markerDaModificare; //marker della geofence da modificare


    public static void setAttivazioneNotifiche(boolean attivazioneNotifiche) {
        Singleton.attivazioneNotifiche = attivazioneNotifiche;
    }
    public static boolean getAttivazioneNotifiche() {
        return attivazioneNotifiche;
    }


    public static void setPosition(LatLng pos) {
        position = pos;
    }

    public static LatLng getPosition() {
        return position;
    }

    public static boolean getModifica() {
        return modifica;
    }

    public static void setModifica(boolean modifica) {
        Singleton.modifica = modifica;
    }

    public static String getGeofDaModificare() {
        return GeofDaModificare;
    }

    public static Marker getMarkerDaModificare() {
        return markerDaModificare;
    }

    public static void setGeofDaModificare(String geofDaModificare) {
        GeofDaModificare = geofDaModificare;
    }

    public static void setMarkerDaModificare(Marker markerDaModificare) {
        Singleton.markerDaModificare = markerDaModificare;
    }

    public static void setElimina(boolean elimina) {
        Singleton.elimina = elimina;
    }

    public static boolean getElimina(){
        return Singleton.elimina;
    }

    public static void setContext(Context context) {
        Singleton.context = context;
    }

    public static Context getContext() {
        return context;
    }

    public static boolean getAttivata() {
        return attivata;
    }

    public static void setAttivata(boolean attivata) {
        Singleton.attivata = attivata;
    }

    public static ArrayList<String> getReport() {
        return report;
    }
}