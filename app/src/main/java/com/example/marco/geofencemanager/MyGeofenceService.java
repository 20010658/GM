package com.example.marco.geofencemanager;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static android.content.ContentValues.TAG;

public class MyGeofenceService extends IntentService {

    DBhelper dBhelper=new DBhelper(Singleton.getContext());

    public MyGeofenceService(){super("Name");}

    protected void onHandleIntent(Intent intent) {
        Log.e("intent","");
        GeofencingEvent geofencingEvent =
                GeofencingEvent.fromIntent(intent);
        // controllo se ci sono errori
        if (geofencingEvent.hasError()) {
            Log.e(TAG, "Errore");
            return;

        }
        //Gestisco geofence "trigger"
        List<Geofence> gfTriggering =
                geofencingEvent.getTriggeringGeofences();

        int gtrType = geofencingEvent.getGeofenceTransition();

        // Get the transition type
        for (Geofence g : gfTriggering) {

            if (gtrType == Geofence.GEOFENCE_TRANSITION_ENTER) {
                notifica(g, 1);
            } else if (gtrType == Geofence.GEOFENCE_TRANSITION_EXIT) {
                notifica(g, 2);
            } else if (gtrType == Geofence.GEOFENCE_TRANSITION_DWELL) {
                Log.e("intent","intent");

                notifica(g, 4);
            }
        }
    }

    //creazione notifica attivazione geofence
    private void notifica(Geofence g, int tipo) {
        //controllo tipo di transizione
        String transizione;
        if (tipo == 1)//geofence entrata
            transizione = g.getRequestId() + "-> " + getString(R.string.enter);
        else if (tipo == 2)//geofence uscita
            transizione = g.getRequestId() + "-> " + getString(R.string.exit);
        else //geofence dwell
            transizione = g.getRequestId() + "-> " + getString(R.string.dwell);
            Log.d("t : ",transizione);
        //Creo notifica
        Calendar c = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy-HH:mm");
        Singleton.getReport().add(df.format(c.getTime())+"     -     "+transizione);
        dBhelper.addTransition();
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(MyGeofenceService.this)
                        .setSmallIcon(android.R.drawable.ic_menu_help)
                        .setContentTitle(getString(R.string.triggeredGeofences))
                        .setContentText(transizione)
                        .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
        Notification mNot = mBuilder.build();
        NotificationManager mNotMan = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotMan != null;
        mNotMan.notify(2, mNot);//2 cosi non sovrascrive la 1 (creazione/modifica geofence)
    }


}