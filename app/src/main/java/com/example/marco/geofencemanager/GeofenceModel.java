package com.example.marco.geofencemanager;

import com.google.android.gms.maps.model.LatLng;


import java.util.ArrayList;

class GeofenceModel {
    private String id;
    private Double lat;
    private Double lon;
    private int time;
    private String raggio;
    private ArrayList<Integer> transizione;
    private boolean attivata;

    GeofenceModel(){
    }

    GeofenceModel(String id, Double lat, Double lon, String raggio,ArrayList<Integer> transizione,int time,boolean attivata){
        this.id=id;
        this.lat=lat;
        this.lon=lon;
        this.raggio=raggio;
        this.transizione=transizione;
        this.time=time;
        this.attivata=attivata;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    public void setAttivata(boolean attivata) {
        this.attivata = attivata;
    }

    public String getRaggio() {
        return raggio;
    }

    public void setRaggio(String raggio) {
        this.raggio = raggio;
    }


    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }



    public ArrayList<Integer> getTransizione() {
        return transizione;
    }

    public void setTransizione(ArrayList<Integer> transizione) {
        this.transizione = transizione;
    }

    public String getId() {return id;}

    public void setId(String id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public LatLng getPos() {
        return new LatLng(this.getLat(),this.getLon());
    }


    public boolean getAttivata() {
        return attivata;
    }
}